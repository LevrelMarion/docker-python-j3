from flask import Flask, request  # pip install flask
import redis
import time

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

@app.route('/', methods=['POST'])
def record():
    request.get_data()
    data = request.data
    timestamp = time.time()
    cache.set(timestamp, str(data))
    return data
    
@app.route('/', methods=['GET'])
def getdata():
    return (str(cache.keys()))
    
if __name__ == "__main__":
    app.run(host= '0.0.0.0')

#commentaire test